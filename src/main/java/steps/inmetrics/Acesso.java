package steps.inmetrics;


import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;
import pom.inmetrics.PageInicial;
import util.Utilitarios;

public class Acesso {

    PageInicial pageInicial = new PageInicial();
    Utilitarios utilitarios = new Utilitarios();

    // Steps dos Cenários

    @Dado("eu acesso a página de login da Inmetrics")
    public void euAcessoAPáginaDeLoginDaInmetrics() {

    }

    @Quando("preencho  o campo Login com {string}")
    public void preenchoOCampoLoginCom(String login) {
        pageInicial.preencheCampoLogin(login);
    }

    @Quando("preencho o campo Senha com {string}")
    public void preenchoOCampoSenhaCom(String senha) {
        pageInicial.preencheCampoSenha(senha);
    }

    @Quando("clico o botão {string}")
    public void clicoOBotão(String botao) {
        pageInicial.clicaBotao(botao);
    }

    @Então("verifico se esta na página principal da Inmetrics")
    public void verificoSeEstaNaPáginaPrincipalDaInmetrics() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }
}






