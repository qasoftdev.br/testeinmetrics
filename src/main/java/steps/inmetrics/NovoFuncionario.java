package steps.inmetrics;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;
import pom.inmetrics.*;
import util.Utilitarios;

public class NovoFuncionario {

    Utilitarios utilitarios = new Utilitarios();
    PageAcesso pageAcesso = new PageAcesso();
    PageCadastro pageCadastro = new PageCadastro();
    PageFuncionario pageFuncionario = new PageFuncionario();
    PageNovoFuncionario pageNovoFuncionario = new PageNovoFuncionario();

    String login = "TesteAce" ;
    String senha = "123456";

//SUCESSO! Usuário cadastrado com sucesso
    @Dado("eu acesso a página de login da Inmetrics e realizo o login")
    public void euAcessoAPáginaDeLoginDaInmetricsERealizoOLogin() {

        String texto = pageAcesso.confirmaPaginaInicial();
        Assert.assertTrue(texto.contains("Login"));
        pageAcesso.preencheCampoLogin(login);
        pageAcesso.preencheCampoSenha(senha);
        pageAcesso.clicaBotao("Entre");
    }

    @Dado("acesso a pagina de cadastro de funcionarios")
    public void acessoAPaginaDeCadastroDeFuncionarios() {
        boolean paginaLogada = pageAcesso.validaPaginaLogada();
        if (paginaLogada){
            System.out.println("Pagina correta carregada");
        } else {
            System.out.println("Pagina incorreta carregada");
        }
    }

    @Dado("clico no link Novo Funcionario")
    public void clicoNoLinkNovoFuncionario() {
        pageFuncionario.clickLinkNovoFuncionario();
    }

    @Quando("carregar a tela,")
    public void carregarATela() {
        boolean paginaCarregada = pageNovoFuncionario.paginaCarregada();
        if (paginaCarregada){
            System.out.println("Pagina correta carregada");
        } else {
            System.out.println("Pagina incorreta carregada");
        }
    }

    @Quando("preencho o campo Nome com o {string} como nome")
    public void preenchoOCampoNomeComOComoNome(String nome) {
       pageNovoFuncionario.preencheInputNome(nome);
    }

    @Quando("preencho o campo CPF com o {string} como CPF")
    public void preenchoOCampoCPFComOComoCPF(String cpf) {
        pageNovoFuncionario.preencheInputCpf(cpf);
    }

    @Quando("preencho o campo cargo com o {string} como Cargo")
    public void preenchoOCampoCargoComOComoCargo(String cargo) {
        pageNovoFuncionario.preencheInputCargo(cargo);
    }

    @Quando("preencho o campo Salario com o {string} como Salário")
    public void preenchoOCampoSalarioComOComoSalário(String salario) {
        pageNovoFuncionario.preencheInputSalario(salario);
    }

    @Quando("Seleciono como opção Sexo como {string} como Sexo")
    public void selecionoComoOpçãoSexoComoComoSexo(String sexo) {
        pageNovoFuncionario.SelecionaSexo(sexo);
    }

    @Quando("preencho o campo Admissão com a {string} como Admissão")
    public void preenchoOCampoAdmissãoComAComoAdmissão(String admissao) {
        pageNovoFuncionario.preencheInputAdmissao(admissao);
    }

    @Quando("seleciono a opção de Tipo de Contratação  como {string} como Tipo de Contratação")
    public void selecionoAOpçãoDeTipoDeContrataçãoComoComoTipoDeContratação(String tpcontratacao) {
        pageNovoFuncionario.checkTipoContratacao(tpcontratacao);
    }

    @Quando("clico no botão {string} para realizar o cadastro do novo funcionário")
    public void clicoNoBotãoParaRealizarOCadastroDoNovoFuncionário(String string) {
        pageNovoFuncionario.ClickBotaoEnviar();
    }

    @Então("verifico se retorna para a página de Funcionarios e apresenta mensagem de sucesso")
    public void verificoSeRetornaParaAPáginaDeFuncionariosEApresentaMensagemDeSucesso() {
        Boolean existe = pageNovoFuncionario.existeMensagemAlert();
        Assert.assertTrue(existe);

        String mensagem = pageNovoFuncionario.retornaMensagemAlert();
        Assert.assertTrue(mensagem.contains("SUCESSO! Usuário cadastrado com sucesso"));
    }


    }
