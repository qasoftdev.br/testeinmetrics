package steps.inmetrics;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;
import pom.inmetrics.PageCadastro;
import util.Utilitarios;

public class Cadastro {

    Utilitarios utilitarios = new Utilitarios();
    PageCadastro pageCadastro = new PageCadastro();



    @Dado("eu acesso a página de login da Inmetrics clico no botão {string}")
    public void euAcessoAPáginaDeLoginDaInmetricsClicoNoBotão(String string) {
        String texto = pageCadastro.confirmaPaginaInicial();
        Assert.assertTrue(texto.contains("Login"));
        pageCadastro.cliqueLinkCadastrar();
    }

    @Quando("preencho  o campo Login com o {string} como usuário")
    public void preenchoOCampoLoginComOComoUsuário(String string) {
        pageCadastro.preencheInputUsuario("testeAce1");
    }

    @Quando("preencho o campo Senha com a {string} como senha")
    public void preenchoOCampoSenhaComAComoSenha(String string) {
        pageCadastro.preencheInputPassword("123456");
    }

    @Quando("preencho o campo Confirme a senha com a {string} como senha")
    public void preenchoOCampoConfirmeASenhaComAComoSenha(String string) {
        pageCadastro.preencheInputConfirmarPassword("123456");
    }

    @Quando("clico o botão {string} para realizar o cadastro")
    public void clicoOBotãoParaRealizarOCadastro(String string) {
        pageCadastro.cliqueBotaoCadastrar();
    }

    @Então("verifico se retorna para a página de Login")
    public void verificoSeRetornaParaAPáginaDeLogin() {
        // Write code here that turns the phrase above into concrete actions
       // throw new cucumber.api.PendingException();
    }

}
