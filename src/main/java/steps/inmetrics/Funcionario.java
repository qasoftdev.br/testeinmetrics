package steps.inmetrics;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;
import pom.inmetrics.PageAcesso;
import pom.inmetrics.PageCadastro;
import pom.inmetrics.PageFuncionario;
import pom.inmetrics.PageNovoFuncionario;
import util.Utilitarios;

public class Funcionario {

    Utilitarios utilitarios = new Utilitarios();
    PageAcesso pageAcesso = new PageAcesso();
    PageCadastro pageCadastro = new PageCadastro();
    PageFuncionario pageFuncionario = new PageFuncionario();
    PageNovoFuncionario pageNovoFuncionario = new PageNovoFuncionario();

    String login = "TesteAce" ;
    String senha = "123456";
    String msgDeletSucesso = "SUCESSO! Funcionário removido com sucesso";
    String msgDeletErro =  "ERRO! Ops.. Não conseguimos remover o funcionário";
    String msgAtualizacaoSucesso = "SUCESSO! Informações atualizadas com sucesso";




    @Dado("eu acesso a página de login da Inmetrics e realizo o login com o usuario")
    public void euAcessoAPáginaDeLoginDaInmetricsERealizoOLoginComOUsuario() {
        String texto = pageAcesso.confirmaPaginaInicial();
        Assert.assertTrue(texto.contains("Login"));
        pageAcesso.preencheCampoLogin(login);
        pageAcesso.preencheCampoSenha(senha);
        pageAcesso.clicaBotao("Entre");
    }

    @Dado("estando na pagina de funcionario")
    public void estandoNaPaginaDeFuncionario() {
        boolean paginaCarregada = pageFuncionario.paginaCarregada();
        if (paginaCarregada){
            System.out.println("Pagina correta carregada");
        } else {
            System.out.println("Pagina incorreta carregada");
        }
    }

    @Quando("preencho o campo pesquisar com o {string}do funcionário")
    public void preenchoOCampoPesquisarComODoFuncionário(String cpf) {
        pageFuncionario.pesquisarFuncionario(cpf);
    }

    @Quando("clico no botão de deletar")
    public void clicoNoBotãoDeDeletar() {
        pageFuncionario.clickBotaoDeletar();
    }

    @Entao("deve apresentar a mensagem de exclusão com sucesso")
    public void deveApresentarAMensagemDeExclusãoComSucesso() {
        Boolean existe = pageNovoFuncionario.existeMensagemAlert();
        Assert.assertTrue(existe);

        String mensagem = pageNovoFuncionario.retornaMensagemAlert();
        Assert.assertTrue(mensagem.contains(msgDeletSucesso));
    }

    //Edição

    @Quando("clico no botão editar")
    public void clicoNoBotãoEditar(){
        pageFuncionario.clickBotaoEditar();
    }

    @Quando("edito o nome do funcionário para {string}")
    public void editoONomeDoFuncionárioPara(String nome) {

        pageNovoFuncionario.preencheInputNome(nome);
    }

    @Quando("edito o sexo para {string}")
    public void editoOSexoPara(String sexo) {
        pageNovoFuncionario.SelecionaSexo(sexo);
    }

    @Quando("o cargo para {string}")
    public void oCargoPara(String cargo) {
        pageNovoFuncionario.preencheInputCargo(cargo);
    }

    @Quando("clico no botão {string} para realizar a edição do  funcionário")
    public void clicoNoBotãoParaRealizarAEdiçãoDoFuncionário(String string) {
        pageNovoFuncionario.ClickBotaoEnviar();
    }

    @Entao("deve apresentar a mensagem de edição com sucesso")
    public void deveApresentarAMensagemDeEdiçãoComSucesso() {
        Boolean existe = pageNovoFuncionario.existeMensagemAlert();
        Assert.assertTrue(existe);

        String mensagem = pageNovoFuncionario.retornaMensagemAlert();
        Assert.assertTrue(mensagem.contains(msgAtualizacaoSucesso));
    }


}
