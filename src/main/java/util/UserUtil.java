package util;


import data.UserData;
import enums.UserEnum;

import java.io.IOException;
import java.util.Properties;

import static enums.Enviroments.HOMOL;
import static enums.Enviroments.PROD;

public class UserUtil {
    private static UserData userData;

    public static UserData setUser(UserEnum type) {
        Properties prop = new Properties();
        userData = new UserData();
        try {
            prop.load(UserUtil.class.getClassLoader().getResourceAsStream("properties/configuration.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (type) {
            case ADM:
                if (HOMOL.equals(Hook.getEnv()) || PROD.equals(Hook.getEnv())) {
                    userData.setUser(prop.getProperty("erp.prod.adm.user"));
                    userData.setPass(prop.getProperty("erp.prod.adm.pass"));
                    userData.setIdEmpresa(prop.getProperty("erp.prod.adm.empresa"));
                    userData.setUserId(prop.getProperty("erp.prod.adm.userId"));
                } else {
                    userData.setUser(prop.getProperty("erp.dev.adm.user"));
                    userData.setPass(prop.getProperty("erp.dev.adm.pass"));
                    userData.setIdEmpresa(prop.getProperty("erp.dev.adm.empresa"));
                    userData.setUserId(prop.getProperty("erp.dev.normal.userId"));
                }
                break;
            case NORMAL:
                if (HOMOL.equals(Hook.getEnv()) || PROD.equals(Hook.getEnv())) {
                    userData.setUser(prop.getProperty("erp.prod.normal.user"));
                    userData.setPass(prop.getProperty("erp.prod.normal.pass"));
                    userData.setIdEmpresa(prop.getProperty("erp.prod.normal.empresa"));
                    userData.setUserId(prop.getProperty("erp.prod.normal.userId"));
                } else {
                    userData.setUser(prop.getProperty("erp.dev.normal.user"));
                    userData.setPass(prop.getProperty("erp.dev.normal.pass"));
                    userData.setIdEmpresa(prop.getProperty("erp.dev.normal.empresa"));
                    userData.setUserId(prop.getProperty("erp.dev.normal.userId"));
                }
                break;
//                Ajustar Parceiros e Contabilidade para padrão do configuration.properties
            case PARCEIRO:
                if (HOMOL.equals(Hook.getEnv()) || PROD.equals(Hook.getEnv())) {
                    userData.setUser(prop.getProperty("erp.prod.normal.user"));
                    userData.setPass(prop.getProperty("erp.prod.normal.pass"));
                    userData.setIdEmpresa(prop.getProperty("erp.prod.normal.empresa"));
                    userData.setUserId(prop.getProperty("erp.prod.normal.userId"));
                } else {
                    userData.setUser(prop.getProperty("erp.dev.normal.user"));
                    userData.setPass(prop.getProperty("erp.dev.normal.pass"));
                    userData.setIdEmpresa(prop.getProperty("erp.dev.normal.empresa"));
                    userData.setUserId(prop.getProperty("erp.dev.normal.userId"));
                }
                break;
            case CONTABILIDADE:
                if (HOMOL.equals(Hook.getEnv()) || PROD.equals(Hook.getEnv())) {
                    userData.setUser(prop.getProperty("erp.prod.normal.user"));
                    userData.setPass(prop.getProperty("erp.prod.normal.pass"));
                    userData.setIdEmpresa(prop.getProperty("erp.prod.normal.empresa"));
                    userData.setUserId(prop.getProperty("erp.prod.normal.userId"));
                } else {
                    userData.setUser(prop.getProperty("erp.dev.normal.user"));
                    userData.setPass(prop.getProperty("erp.dev.normal.pass"));
                    userData.setIdEmpresa(prop.getProperty("erp.dev.normal.empresa"));
                    userData.setUserId(prop.getProperty("erp.dev.normal.userId"));
                }
                break;
        }
        return userData;
    }

    public static UserData getUserData() {
        return userData;
    }
}

