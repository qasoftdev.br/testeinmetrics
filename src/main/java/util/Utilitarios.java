package util;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Utilitarios {

    public Utilitarios(){
        Hook.getDriver().get(Hook.getBaseUrl());
        PageFactory.initElements(Hook.getDriver(), this);
    }



    public static void screenShot( String nomeClasse) throws IOException {

        Locale locale = new Locale("pt","BR");
        GregorianCalendar calendar = new GregorianCalendar();
        SimpleDateFormat formatador = new SimpleDateFormat("dd'_de_'MMMMM'_de_'yyyy'-'HH'_'mm'h'",locale);
        System.out.println(formatador.format(calendar.getTime()));
        String data = formatador.format(calendar.getTime());

        File scrFile = ((TakesScreenshot)Hook.getDriver()).getScreenshotAs(OutputType.FILE);
        // Now you can do whatever you need to do with it, for example copy somewhere
        FileUtils.copyFile(scrFile, new File("c:\\tmp\\"+nomeClasse+"_scre" +
                ""));
    }
}
