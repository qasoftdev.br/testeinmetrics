package util.dbActions;

import util.DbConnection;

import java.sql.SQLException;

public class dbCaixas extends DbConnection {


    public static void updateCaixaLixeira(String idEmpresa) throws SQLException {
        connectionDb();
        stmt = conndb.createStatement();
        stmt.executeUpdate(
                "update nfe_frente_caixa_pdv\n" +
                        "set lixeira = 'sim'\n" +
                        "where id_empresa = " + idEmpresa + "\n" +
                        "and lixeira = 'nao'");
        stmt.close();
        conndb.close();
    }

    public static void setEmpresaOldClient(String idEmpresa) throws SQLException {
        connectionDb();
        stmt = conndb.createStatement();
        stmt.executeUpdate(
                "update nfe_config_empresas_parametros\n" +
                        "set pdv_caixa = 1\n" +
                        "where id_empresa = " + idEmpresa + "\n");
        stmt.close();
        conndb.close();
    }

    public static void setEmpresaNewClient(String idEmpresa) throws SQLException {
        connectionDb();
        stmt = conndb.createStatement();
        stmt.executeUpdate(
                "update nfe_config_empresas_parametros\n" +
                        "set pdv_caixa = NULL\n" +
                        "where id_empresa = " + idEmpresa + "\n");
        stmt.close();
        conndb.close();
    }

    public static void createCashierDb(String idEmpresa) throws SQLException {
        connectionDb();
        stmt = conndb.createStatement();
        stmt.executeUpdate(
                "Insert into nfe_frente_caixa_pdv (id_empresa, nome, valor_max_per, status) " +
                        "values (" + idEmpresa + ", 'ReforcoCaixa', '1','Aberto');");
    }

    public static void createLimitedCashierDb(String idEmpresa) throws SQLException {
        connectionDb();
        stmt = conndb.createStatement();
        stmt.executeUpdate(
                "Insert into nfe_frente_caixa_pdv (id_empresa, nome, valor_min, valor_min_per, valor_max, valor_max_per, status) " +
                        "values (" + idEmpresa + ", 'ReforcoCaixa', '100', '0', '2000', '0','Aberto');");
    }

    public static void addCashierMoney(String idEmpresa, String userId) throws SQLException {
        connectionDb();
        stmt = conndb.createStatement();
        rs = stmt.executeQuery(
                "SELECT id_pdv FROM nfe_frente_caixa_pdv where id_empresa = " + idEmpresa + " " +
                        "order by id_pdv desc limit 1;");
        Integer inter = null;
        while (rs.next()) {
            inter = rs.getInt(1);
        }
        stmt.executeUpdate("Insert into nfe_frente_caixa_pdv_movimentacao " +
                "(id_empresa, id_pdv, id_usuario, tipo, acao, moeda, valor)" +
                "values(" + idEmpresa + "," + inter + "," + userId + ",'Entrada','Reforco', 'Dinheiro', '16000.00');");
    }

    public static void createCloseCashierDb(String idEmpresa) throws SQLException {
        connectionDb();
        stmt = conndb.createStatement();
        stmt.executeUpdate(
                "Insert into nfe_frente_caixa_pdv (id_empresa, nome, valor_max_per, status) " +
                        "values (" + idEmpresa + ", 'CaixaQueEstaFechado', '1','Fechado');");
    }

    public static void usrReinforcePermission(String userId) throws SQLException {
        connectionDb();
        stmt.executeUpdate(
                "update nfe_frente_caixa_pdv_permissao_usuario set reforco = '0'" +
                        " where id_usuario = " + userId + ";");
    }

    public static void addUsrToCashier(String idEmpresa, String userId) throws SQLException {
        connectionDb();
        stmt = conndb.createStatement();
        rs = stmt.executeQuery(
                "SELECT id_pdv FROM nfe_frente_caixa_pdv where id_empresa = " + idEmpresa + " " +
                        "order by id_pdv desc limit 1;");
        Integer inter = null;
        while (rs.next()) {
            inter = rs.getInt(1);
        }
        stmt.executeUpdate(
                "insert into nfe_frente_caixa_pdv_usuario (id_pdv, id_usuario) " +
                        "values(" + inter + "," + userId + ")");
    }
}
