package util;



import enums.Browser;
import enums.Enviroments;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Hook {

    protected static WebDriver driver;
    protected static Enviroments env;
    protected static String baseURL;

    @Before
    public void before() {
        String b = System.getProperty("browser");
        if (null == b)
           // b = "default";
          //  b = "EDGE";
            b = "CHROME";

        Browser browser = Browser.getBrowser(b.toUpperCase());
        switch (browser) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                ChromeOptions op = new ChromeOptions();
               // op.addArguments("--headless");
                //colocar dentro do argumento --headless
                driver = new ChromeDriver(op);
                break;
            case EDGE:
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                break;

            default:
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions fop = new FirefoxOptions();
                //fop.addArguments("");
                //colocar dentro do argumento --headless
                driver = new FirefoxDriver(fop);

        }
        driver.manage().window().maximize();
        String e = System.getProperty("env");
        if (null == e)
            //e = "HOMOL";
           // e = "DEV";
            //e = "CONTAINER";
            e = "PROD";

        Enviroments env = Enviroments.getEnv(e.toUpperCase());
        switch (env) {
            case DEV:
                baseURL = "";
                break;
            case PROD:
                baseURL = "http://www.inmrobo.tk/accounts/login/";
                break;
            case HOMOL:
                baseURL = "";
                break;
            case CONTAINER:
                baseURL = "";
                break;
            default:
                baseURL = "";
        }
    }

    @After(order = 1)
    public void screenshot(Scenario scenario) {
        Locale locale = new Locale("pt","BR");
        GregorianCalendar calendar = new GregorianCalendar();
        SimpleDateFormat formatador = new SimpleDateFormat("dd'_de_'MMMMM'_de_'yyyy'-'HH'_'mm'h'",locale);
        System.out.println(formatador.format(calendar.getTime()));
        String data = formatador.format(calendar.getTime());
        File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            //FileUtils.copyFile(file, new File("c:\\tmp\\screenshots\\srcshot_" + data+ ".jpg"));
            FileUtils.copyFile(file, new File("target\\screenshots\\srcshot_" + data+ ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @After(order = 0)
    public void after() {
        driver.quit();
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static String getBaseUrl() {
        return baseURL;
    }

    public static Enviroments getEnv() {
        return env;
    }

}
