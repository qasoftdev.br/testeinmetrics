package util;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Statement;
import java.util.ArrayList;

public class Tools {
    Statement stmt = null;


    public static WebElement waitBy(WebElement element) {
        return waitBy(element, 40);
    }

    public static WebElement waitBy(WebElement element, Integer time) {
        screenLoadWait();
        try {
            new WebDriverWait(Hook.driver, time)
                    .until(ExpectedConditions.visibilityOf(element));
        } catch (Exception e) {
            System.err.println(element + "Não encontrado....");
        }
        return element;
    }

    public static WebElement waitForInvisibilty(WebElement element, Integer time) {
        screenLoadWait();
        try {
            new WebDriverWait(Hook.driver, time)
                    .until(ExpectedConditions.invisibilityOf(element));
        } catch (Exception e) {
            System.err.println(element + "Elemento Travou na tela....");
        }
        return element;
    }

    public static WebElement waitFilledBy(WebElement element, String text) {
        screenLoadWait();
        try {
            new WebDriverWait(Hook.driver, 5)
                    .until(ExpectedConditions.textToBePresentInElementValue(element, text));
        } catch (Exception e) {
            System.err.println(element + "Não encontrado....");
        }
        return element;
    }

    public static WebElement scrollDownTo(WebElement element) {
        waitBy(element);
        Actions actions = new Actions(Hook.driver);
        actions.moveToElement(element);
        actions.perform();
        return element;
    }

    public static void acceptAlert() {
        try {
            Alert alert = Hook.driver.switchTo().alert();
            alert.accept();
        } catch (Exception e) {
            System.err.println("Alert não apareceu");
        }
    }

    public static void clickIfPresent(WebElement element) {
        try {
            waitBy(element, 5).click();
        } catch (Exception e) {
        }
    }

    public static void screenLoadWait() {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(Hook.driver, 60);
        wait.until(pageLoadCondition);

    }

    public static void changeWindow() {
        ArrayList<String> tabs = new ArrayList<String>(Hook.driver.getWindowHandles());
        Hook.driver.switchTo().window(tabs.get(1).toString());
    }

}
