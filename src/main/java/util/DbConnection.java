package util;

import java.sql.*;

public class DbConnection {
    //private static Connection conn = null;
    protected static Statement stmt = null;
    protected static ResultSet rs = null;
    protected static Connection conndb = null;

    public static Connection connectionDb() {
        String dbEnv = null;
        try {
            if (System.getProperty("env").equals("prod") || System.getProperty("env").equals("homolog")) {
                dbEnv = "<AGUARDANDO PRESENTE>";
            } else {
                dbEnv = "jdbc:mysql://52.67.35.227/vhsys_erp_dev?user=root&password=pfn4$6ns-2DD";
            }
            conndb = DriverManager.getConnection(dbEnv);
        } catch (
                SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
        }

        return conndb;
    }

}