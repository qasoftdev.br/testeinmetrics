package pom.inmetrics;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import util.Hook;
import util.Tools;


import static util.Tools.waitBy;

public class PageAcesso {


    public PageAcesso(){
        Hook.getDriver().get(Hook.getBaseUrl());
        PageFactory.initElements(Hook.getDriver(), this);
    }

// String para acesso rapido
    String loginConteudo ="";
    String senhaConteudo="";


    //Mapeamento dos Campos
    // Tela acesso
    @FindBy(how = How.CLASS_NAME,using = "login100-form-title")
    private WebElement titulo;
    @FindBy(how = How.NAME,using = "username")
    private WebElement inputUsuario;
    @FindBy(how = How.NAME,using = "pass")
    private WebElement inputPassword;
    @FindBy(how = How.CLASS_NAME,using = "login100-form-btn")
    private WebElement botaoEntre;
    @FindBy(how = How.CLASS_NAME,using = "txt2 bo1")
    private WebElement linkCadastrese;
    @FindBy(how = How.ID,using = "tabela")
    private WebElement validapaginaLogada;
    @FindBy(how = How.ID,using = "alert alert-danger")
    private WebElement alertErro;
    //ERRO! Usuário ou Senha inválidos



    public String confirmaPaginaInicial(){
     return Tools.waitBy(titulo).getText();

    }
    public void preencheCampoLogin(String loginCampo){
        Tools.waitBy(inputUsuario ,10).click();
        Tools.waitBy(inputUsuario ,10).sendKeys(loginCampo);

    }
    public void preencheCampoSenha(String senhaCampo){
        Tools.waitBy(inputPassword ,10).click();
        Tools.waitBy(inputPassword ,10).sendKeys(senhaCampo);
    }

    public void clicaBotao(String botao){
        if(botao.equals("Entre"))
            Tools.waitBy(botaoEntre,10).click();
    }

    public Boolean validaPaginaLogada()  {

        Tools.waitBy(validapaginaLogada,100).isDisplayed();
        return Tools.waitBy(validapaginaLogada,100).isDisplayed();
    }

//    public boolean atributoBotalLogin(){
//        if(Tools.waitBy(botaoLogar).isEnabled())
//            return true;
//        else
//            return false;
//
//    }

}
