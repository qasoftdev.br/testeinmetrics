package pom.inmetrics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import util.Hook;
import util.Tools;

public class PageNovoFuncionario {
    public PageNovoFuncionario(){
        //Hook.getDriver().get(Hook.getBaseUrl());
        PageFactory.initElements(Hook.getDriver(), this);
    }

    @FindBy(how = How.ID,using = "inputNome")
    private WebElement inputNome;

    @FindBy(how = How.ID,using = "cpf")
    private WebElement inputCpf;

    @FindBy(how = How.ID,using = "inputCargo")
    private WebElement inputCargo;

    @FindBy(how = How.ID,using = "dinheiro")
    private WebElement inputSalario;

    @FindBy(how = How.ID,using = "inputAdmissao")
    private WebElement inputAdmissao;

    @FindBy(how = How.ID,using = "slctSexo")
    private WebElement selectSexo;

    @FindBy(how = How.ID,using = "clt")
    private WebElement radioClt;

    @FindBy(how = How.ID,using = "pj")
    private WebElement radioPj;

    @FindBy(how = How.CLASS_NAME,using = "cadastrar-form-btn")
    private WebElement botaoEnviar;

    @FindBy(how = How.CLASS_NAME,using = "cancelar-form-btn")
    private WebElement botaoCancelar;

    @FindBy(how = How.CLASS_NAME, using = "alert-success")
    private WebElement validaCadastrado;


    public boolean paginaCarregada() {
       return  Tools.waitBy(inputNome ,10).isDisplayed();
    }

    public void preencheInputNome(String nome){
        Tools.waitBy(inputNome ,100).click();
        Tools.waitBy(inputNome ,100).clear();
        Tools.waitBy(inputNome ,10).sendKeys(nome);
    }

    public void preencheInputCpf(String cpf){
        Tools.waitBy(inputCpf ,10).click();
        Tools.waitBy(inputCpf ,10).clear();
        Tools.waitBy(inputCpf ,10).sendKeys(cpf);
        Tools.waitBy(inputCpf ,10).click();
    }

    public void preencheInputCargo(String cargo){
        Tools.waitBy(inputCargo ,10).click();
        Tools.waitBy(inputCargo ,10).clear();
        Tools.waitBy(inputCargo ,10).sendKeys(cargo);
    }

    public void preencheInputSalario(String salario){
        Tools.waitBy(inputSalario ,10).click();
        Tools.waitBy(inputSalario ,10).clear();
        Tools.waitBy(inputSalario ,10).sendKeys(salario);
    }

    public void preencheInputAdmissao(String admissao){
        Tools.waitBy(inputAdmissao ,10).click();
        Tools.waitBy(inputAdmissao ,10).clear();
        Tools.waitBy(inputAdmissao ,10).sendKeys(admissao);
    }

    public void SelecionaSexo(String sexoEscolhido) {
//        Select sexo = (Select) Tools.scrollDownTo(selectSexo);
//        sexo.selectByVisibleText("Masculino");

        Select sexo = new  Select(Hook.getDriver().findElement(By.id("slctSexo")));
        sexo.selectByVisibleText(sexoEscolhido);

    }

    public void checkTipoContratacao(String contratacao){
        if(contratacao.equals("CLT")){
            Tools.waitBy(radioClt).click();
        } else {
            Tools.waitBy(radioPj).click();
        }
    }

    public void ClickBotaoEnviar() {
        Tools.waitBy(botaoEnviar,20).click();
    }

    public String retornaMensagemAlert() {
       return Tools.waitBy(validaCadastrado).getText();
    }

    public Boolean existeMensagemAlert() {
      return Tools.waitBy(validaCadastrado,20).isDisplayed();
    }


}
