package pom.inmetrics;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import util.Hook;
import util.Tools;

import javax.tools.Tool;

public class PageFuncionario {

    public PageFuncionario(){
        //Hook.getDriver().get(Hook.getBaseUrl());
        PageFactory.initElements(Hook.getDriver(), this);
    }

    @FindBy(how = How.XPATH,using = "//a[@href='/empregados/new_empregado']")
    private WebElement novoFuncionario;
    @FindBy(how = How.XPATH,using = "//input[@type='search']")
    private WebElement inputPesquisar;
    @FindBy(how = How.ID,using = "delete-btn")
    private WebElement botaoDeletar;
    @FindBy(how = How.CLASS_NAME,using = "fa-pencil")
    private WebElement botaoEditar;
    @FindBy(how = How.CLASS_NAME, using = "alert-success")
    private WebElement validaCadastrado;


    //SUCESSO! Funcionário removido com sucesso
    //ERRO! Ops.. Não conseguimos remover o funcionário

    public void clickLinkNovoFuncionario(){
        Tools.waitBy(novoFuncionario).click();
    }

    public void pesquisarFuncionario(String cpf){
        Tools.waitBy(inputPesquisar,10).click();
        Tools.waitBy(inputPesquisar,10).sendKeys(cpf);
        Tools.waitForInvisibilty(inputPesquisar,10);
    }

    public void clickBotaoDeletar(){
        Tools.waitBy(botaoDeletar).click();
    }

    public void clickBotaoEditar(){
        Tools.waitBy(botaoEditar).click();
    }

    public Boolean paginaCarregada() {
        return Tools.waitBy(inputPesquisar,20).isDisplayed();
    }

    public String retornaMensagemAlert() {
        return Tools.waitBy(validaCadastrado).getText();
    }

    public Boolean existeMensagemAlert() {
        return Tools.waitBy(validaCadastrado,20).isDisplayed();
    }

}
