package pom.inmetrics;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import util.Hook;
import util.Tools;

public class PageCadastro {


    public PageCadastro(){
        //Hook.getDriver().get(Hook.getBaseUrl());
        PageFactory.initElements(Hook.getDriver(), this);
    }

    @FindBy(how = How.CLASS_NAME,using = "login100-form-title")
    private WebElement titulo;
    @FindBy(how = How.NAME,using = "username")
    private WebElement inputUsuario;
    @FindBy(how = How.NAME,using = "pass")
    private WebElement inputPassword;
    @FindBy(how = How.NAME,using = "confirmpass")
    private WebElement inputConfirmarPassword;
    @FindBy(how = How.CLASS_NAME,using = "login100-form-btn")
    private WebElement botaoCadastrar;
    @FindBy(how = How.CLASS_NAME,using = "txt2")
    private WebElement linkLogin;
    @FindBy(how = How.CLASS_NAME,using = "txt2")
    private WebElement linkCadastrar;


    public  void preencheInputUsuario(String nome){
        Tools.waitBy(inputUsuario ,10).click();
        Tools.waitBy(inputUsuario ,10).sendKeys(nome);
    }

    public  void preencheInputPassword(String senha){
        Tools.waitBy(inputPassword ,10).click();
        Tools.waitBy(inputPassword ,10).sendKeys(senha);
    }

    public  void preencheInputConfirmarPassword(String senha){
        Tools.waitBy(inputConfirmarPassword ,10).click();
        Tools.waitBy(inputConfirmarPassword ,10).sendKeys(senha);
    }

    public void cliqueBotaoCadastrar(){
        Tools.waitBy(botaoCadastrar, 10).click();
    }

    public void cliqueLinkCadastrar(){
        Tools.waitBy(linkCadastrar, 10).click();
    }

    public void cliqueLinkLogin(){
        Tools.waitBy(linkLogin, 10).click();
    }

    public String confirmaPaginaInicial(){
        return Tools.waitBy(titulo).getText();

    }

}
