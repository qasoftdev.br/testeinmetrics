package pom.inmetrics;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import util.Hook;
import util.Tools;

public class PageInicial {

    public PageInicial(){
        Hook.getDriver().get(Hook.getBaseUrl());
        PageFactory.initElements(Hook.getDriver(), this);
    }

    @FindBy(how = How.CLASS_NAME,using = "logo")
    private WebElement logo;
    @FindBy(how = How.ID,using = "mat-input-0")
    private WebElement login;
    @FindBy(how = How.ID,using = "mat-input-1")
    private WebElement senha;
    @FindBy(how = How.CLASS_NAME,using = "submit-button")
    private WebElement botaoLogar;
    @FindBy(how = How.ID,using = "login-form")
    private WebElement validaLoginForm;
    @FindBy(how = How.CLASS_NAME,using = "title")
    private WebElement validaPaginaLogada;


    public boolean confirmaPaginaInicial(){
        if(Tools.waitBy(validaLoginForm).isDisplayed())
         return true;
        else
            return  false;

    }
    public void preencheCampoLogin(String loginCampo){
        Tools.waitBy(login ,10).click();
        Tools.waitBy(login ,10).sendKeys(loginCampo);

    }
public void preencheCampoSenha(String senhaCampo){
    Tools.waitBy(senha ,10).click();
    Tools.waitBy(senha ,10).sendKeys(senhaCampo);
    }

    public void clicaBotao(String botao){
        if(botao.equals("LOGIN"))
            Tools.waitBy(botaoLogar,10).click();
    }

    public String validaPaginaPrincipal()  {

        Tools.waitBy(validaPaginaLogada,100).isDisplayed();
        return Tools.waitBy(validaPaginaLogada,100).getText();
    }

    public boolean atributoBotalLogin(){
        if(Tools.waitBy(botaoLogar).isEnabled())
            return true;
        else
            return false;

    }


}
