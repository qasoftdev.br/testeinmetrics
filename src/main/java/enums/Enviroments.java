package enums;

public enum Enviroments {
    LOCAL("LOCAL"),
    DEV("DEV"),
    HOMOL("HOMOLOG"),
    PROD("PROD"),
    CHECLOCAL("CHECLOCAL"),
    CHECPROD("CHECPROD"),
    ERPLOCAL("ERPLOCAL"),
    CONTAINER("CONTAINER");

    Enviroments(String env) {
    }

    public static Enviroments getEnv(String env) {
        if (env.equals(LOCAL.name()))
            return LOCAL;
        else if (env.equals(DEV.name()))
            return DEV;
        else if (env.equals(HOMOL.name()))
            return HOMOL;
        else if (env.equals(CHECLOCAL.name()))
            return CHECLOCAL;
        else if (env.equals(CHECPROD.name()))
            return CHECPROD;
        else if (env.equals(ERPLOCAL.name()))
            return ERPLOCAL;
        else if (env.equals(CONTAINER.name()))
            return CONTAINER;
        else
            return PROD;
    }
}

