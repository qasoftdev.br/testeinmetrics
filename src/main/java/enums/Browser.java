package enums;

public enum Browser {
    EDGE("EDGE"),
    CHROME("CHROME"),
    DEFAULT("FIREFOX");

    Browser(String browser) {
    }

    public static Browser getBrowser(String browser) {

        if (browser.equals(EDGE.name()))
            return EDGE;
        else if (browser.equals(CHROME.name()))
            return CHROME;
        else
            return DEFAULT;
    }
}

