package enums;

public enum UserEnum {
    ADM("ADM"),
    NORMAL("NORMAL"),
    PARCEIRO("PARCEIRO"),
    CONTABILIDADE("CONTABILIDADE");

    private String user;

    UserEnum(String user) {
        this.user = user;
    }

    public String getUser() {
        return this.user;
    }
}
