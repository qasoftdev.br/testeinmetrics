import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import com.github.mkolisnyk.cucumber.runner.ExtendedTestNGRunner;

//@RunWith(ExtendedCucumber.class)
//@RunWith(ExtendedCucumber.class)
@RunWith(Cucumber.class)


//@ExtendedCucumberOptions(jsonReport = "target/cucumber.json",
//                        retryCount = 3,
//                        detailedReport = true,
//                        detailedAggregatedReport = true,
//                        overviewReport = true,
//                        //coverageReport = true,
//                        jsonUsageReport = "target/cucumber-usage.json",
//                        usageReport = true,
//                        toPDF = true,
//                        excludeCoverageTags = {"@flaky" },
//                        includeCoverageTags = {"@passed" },
//                        outputFolder = "target/pdf/")

@CucumberOptions
        (

                features = "src/main/resources/features/inmetrics/imetrics.feature",
                //tags = {"@checkout"},
                glue = {"src","steps", "util"},
                plugin = {
                        "pretty",
                        "json:target/cucumber.json",
                        "html:target/cucumber-html-report",
                        "json:target/cucumber.json",
                       "pretty:target/cucumber-pretty.txt",
                        //"usage:target/cucumber-usage.json",
                        "junit:target/cucumber-results.xml"
                },

                       // {"pretty","json:target/cucumber.json", "html:target/report-html"},
                dryRun = false,
                strict = false,
                snippets = CucumberOptions.SnippetType.CAMELCASE
        )
public class TestRunner {

}