# language: pt

# Cenários de validação de funcionalidade Cadastrar usuário
#Funcionalidade: Validar a funcionalidade Cadastrar usuário

######################################## Cadastrar usuário  ########################################

Funcionalidade: Testes Inmetrics

#  Cenário 01
  Cenário: Executar o cadastro de um  usuário
    Dado eu acesso a página de login da Inmetrics clico no botão "Cadastrese"
    Quando preencho  o campo Login com o "testeInmetrics02" como usuário
    E preencho o campo Senha com a "01020304" como senha
    E preencho o campo Confirme a senha com a "01020304" como senha
    E clico o botão "Cadastrar" para realizar o cadastro
    Então verifico se retorna para a página de Login

#  Cenário 02
  Cenário: Executar o login-
    Dado eu acesso a página de login da Inmetrics
    Quando preencho  o campo Login com "testeInmetrics02"
    E preencho o campo Senha com "01020304"
    E clico o botão "Entre"
    Então verifico se esta na página principal da Inmetrics


#  Cenário 03
  Cenário: Executar o cadastro de um  Funcionário
    Dado eu acesso a página de login da Inmetrics e realizo o login
    E acesso a pagina de cadastro de funcionarios
    E clico no link Novo Funcionario
    Quando carregar a tela,
    E preencho o campo Nome com o "André João da Silva" como nome
    E preencho o campo CPF com o "837.632.299-06" como CPF
    E preencho o campo cargo com o "Desenvolvedor" como Cargo
    E preencho o campo Salario com o "2900.,00" como Salário
    E Seleciono como opção Sexo como "Masculino" como Sexo
    E preencho o campo Admissão com a "21/07/2020" como Admissão
    E seleciono a opção de Tipo de Contratação  como "PJ" como Tipo de Contratação
    E clico no botão "Enviar" para realizar o cadastro do novo funcionário
    Então verifico se retorna para a página de Funcionarios e apresenta mensagem de sucesso

#  Cenário 04
  Cenário: Executar a edição de um  Funcionário
    Dado eu acesso a página de login da Inmetrics e realizo o login com o usuario
    E estando na pagina de funcionario
    Quando preencho o campo pesquisar com o "837.632.299-06"do funcionário
    E clico no botão editar
    E edito o nome do funcionário para "Andréa Joana da Silva"
    E edito o sexo para "Feminino"
    E o cargo para "Faxineiro"
    E clico no botão "Enviar" para realizar a edição do  funcionário
    Entao deve apresentar a mensagem de edição com sucesso

#  Cenário 05
  Cenário: Executar a exclusão de um  Funcionário
    Dado eu acesso a página de login da Inmetrics e realizo o login com o usuario
    E estando na pagina de funcionario
    Quando preencho o campo pesquisar com o "837.632.299-06"do funcionário
    E clico no botão de deletar
    Entao deve apresentar a mensagem de exclusão com sucesso


